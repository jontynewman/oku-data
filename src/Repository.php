<?php

namespace JontyNewman\Oku\Data;

use ArrayAccess;
use InvalidArgumentException;
use JontyNewman\Oku\ResponseBuilderInterface;
use Psr\Http\Message\ServerRequestInterface;
use UnexpectedValueException;

class Repository implements ArrayAccess {

	const OPTIONS = 0;

	const DEPTH = 512;

	private $array;

	private $callback;

	private $convert;

	public static function json(
			int $options = self::OPTIONS,
			int $depth = self::DEPTH
	): callable {

		return function ($data) use ($options, $depth): callable {

			return function (
					ResponseBuilderInterface $builder
			) use ($data, $options, $depth): void {

			$builder->header('Content-Type', 'application/json');
			$builder->content(function () use ($data, $options, $depth) {

				echo json_encode($data, $options, $depth);
			});
		};
		};
	}

	public static function whitelist(array $whitelist): callable {

		$keyed = array_fill_keys($whitelist, null);

		return function (ServerRequestInterface $request) use ($keyed) {

			$filtered = null;
			$data = $request->getParsedBody();

			if (is_array($data)) {
				$filtered = array_intersect_key($data, $keyed);
			} elseif (is_object($data)) {
				$filtered = (object) array_intersect_key((array) $data, $keyed);
			}

			return $filtered;
		};
	}

	public function __construct(
			ArrayAccess $array,
			callable $callback,
			callable $convert = null
	) {

		$this->array = $array;
		$this->callback = $callback;
		$this->convert = $convert ?? function (
				ServerRequestInterface $request
		) {
			return $request->getParsedBody();
		};
	}


	public function offsetExists($offset): bool {

		return $this->array->offsetExists($offset);
	}

	public function offsetGet($offset) {

		return ($this->callback)($this->array->offsetGet($offset));
	}

	public function offsetSet($offset, $value): void {

		if (!($value instanceof ServerRequestInterface)) {
			throw new InvalidArgumentException('Expected value to be a PSR-compliant server request');
		}

		$this->array->offsetSet($offset, ($this->convert)($value));
	}

	public function offsetUnset($offset): void {

		$this->array->offsetUnset($offset);
	}
}
